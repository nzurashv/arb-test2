const touchCoordinatesDiv = document.getElementById('touchCoordinates')

let initialDistance = 0
const MIN_DISTANCE_THRESHOLD = 10
const zoomSpeed = 0.05

const zoomIn = function (model) {
  const prevSvale = model.getAttribute('scale')
  const newScale = {
    x: prevSvale.y + zoomSpeed,
    y: prevSvale.x + zoomSpeed,
    z: prevSvale.z + zoomSpeed,
  }
  if (newScale.x > 2 || newScale.y > 2 || newScale.z > 2) return
  model.setAttribute('scale', newScale)
}
const zoomOut = function (model) {
  const prevSvale = model.getAttribute('scale')
  const newScale = {
    x: prevSvale.y - zoomSpeed,
    y: prevSvale.x - zoomSpeed,
    z: prevSvale.z - zoomSpeed,
  }
  if (newScale.x <= 0.1 || newScale.y <= 0.1 || newScale.z <= 0.1) return
  model.setAttribute('scale', newScale)
}

AFRAME.registerComponent('rotate-on-touch', {
  init: function () {
    this.rotationSpeed = 0.05
    this.isZooming = false
    this.el.sceneEl.addEventListener('touchstart', this.onTouchStart.bind(this))
    this.el.sceneEl.addEventListener('touchmove', this.onTouchMove.bind(this))
    this.el.sceneEl.addEventListener('touchend', this.onTouchEnd.bind(this))

    this.modelRotation = { x: 0, y: -90 }
  },

  onTouchStart: function (event) {
    if (event.touches.length === 1) {
      this.isZooming = false
      this.touchStartX = event.touches[0].pageX
      this.touchStartY = event.touches[0].pageY
      this.rotationStart = null
    } else if (event.touches.length === 2) {
      this.isZooming = true
      const touch1 = event.touches[0]
      const touch2 = event.touches[1]
      initialDistance = Math.hypot(
        touch2.pageX - touch1.pageX,
        touch2.pageY - touch1.pageY,
      )
    }
  },

  onTouchMove: function (event) {
    if (event.touches.length === 1 && !this.isZooming) {
      const touchX = event.touches[0].pageX
      const touchY = event.touches[0].pageY

      let [rotationX, rotationY] = [this.modelRotation.x, this.modelRotation.y]

      const deltaX = touchX - this.touchStartX
      const deltaY = touchY - this.touchStartY

      if (this.rotationStart) {
        if (this.rotationStart == 'x') {
          rotationY += deltaX * this.rotationSpeed
        } else rotationX += deltaY * this.rotationSpeed
      } else {
        this.rotationStart = Math.abs(deltaX) > Math.abs(deltaY) ? 'x' : 'y'
      }
      if (rotationX < 0) rotationX = 360 + rotationX
      if (rotationY < 0) rotationY = 360 + rotationY
      this.modelRotation = {
        x: rotationX,
        y: rotationY,
      }
      const newRotationQuaternion = new THREE.Quaternion().setFromEuler(
        new THREE.Euler(
          0,
          rotationY * (Math.PI / 180),
          -rotationX * (Math.PI / 180),
          'YXZ',
        ),
      )

      this.el.object3D.setRotationFromQuaternion(newRotationQuaternion)
      // this.el.setAttribute('rotation', { x: rotationX, y: rotationY, z: 0 })
    } else if (event.touches.length === 2) {
      const touch1 = event.touches[0]
      const touch2 = event.touches[1]
      const currentDistance = Math.hypot(
        touch2.pageX - touch1.pageX,
        touch2.pageY - touch1.pageY,
      )

      const distanceChange = currentDistance - initialDistance

      if (Math.abs(distanceChange) >= MIN_DISTANCE_THRESHOLD) {
        if (distanceChange > 0) {
          zoomIn(this.el)
        } else {
          zoomOut(this.el)
        }

        initialDistance = currentDistance
      }
    }
  },
  onTouchEnd: function (event) {
    if (event.touches.length === 0) {
      this.isZooming = false
    }
  },
})

const exampleTarget = document.querySelector('#target')
exampleTarget.addEventListener('targetFound', (event) => {
  console.log('target found')
})
